#/bin/sh
set -e

# Clean
rm -rf grimoire
rm -rf frontend

# Clone the original application repository.
git clone --branch dev --depth 1 https://gitlab.com/benjamin.lemelin/grimoire.git grimoire
mv grimoire/frontend frontend
cd frontend

# Change API to use demo mode.
sed -i "s@export \* from \"./normal\";@export \* from \"./demo\";@g" src/api/index.ts

# Build the demo application.
npm ci
npm run build -- --base=/${CI_PROJECT_NAME}