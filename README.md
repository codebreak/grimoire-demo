<div align="center">

# ![Grimoire](https://gitlab.com/benjamin.lemelin/grimoire/-/raw/main/.docs/icon-large.svg)

</div>

Grimoire is a recipe management application that allows users to create and store their own unique recipes. Users can
easily write out their favorite recipes, including ingredients and preparation steps, and view them all in one
convenient location. Whether you're a seasoned chef or a beginner cook, Grimoire is the perfect tool for organizing and
keeping track of all your favorite recipes.

<div align="center">

![Grimoire Home Page](https://gitlab.com/benjamin.lemelin/grimoire/-/raw/main/.docs/screenshot.png)

</div>

## What is this ?

This is the demo version of the Grimoire, which you can access [here][Demo] on GitLab Pages. It enables you to evaluate
the application before installing it on your own system. The demo offers limited functionality, but it provides an
opportunity to preview its capabilities.

When prompted to log in, please use `admin` for both the username and password. Additionally, there are fake `user`
and `guest` accounts available for previewing the appearance and features associated with those account types.

If you are interested in learning more, please check the application's [Git repository][Repository].

## License

This project is licensed under the GNU GPLv3. See the [LICENSE.md](LICENSE.md) file for details.

[Repository]: https://gitlab.com/benjamin.lemelin/grimoire

[Demo]: https://codebreak.gitlab.io/grimoire-demo
